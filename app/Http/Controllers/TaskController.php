<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Task;
use Cache;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class TaskController extends Controller
{
    public function getAllTasks(Request $request)
    {
        return response()->json(['tasks' => Task::all()]);
    }

    public function getOneTask($id)
    {
        try {
            $task = Task::findOrFail($id);

            return response()->json($task);
        } catch (ModelNotFoundException $exception) {
            return response()->json(['error' => "Task with id {$id} not found."], 404);
        }
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'name'        => 'string|required',
            'description' => 'string|required',
            'done'        => 'required|in:0,1',
            'priority'    => 'required|in:high,medium,low'
        ]);

        $task = Task::create($request->all());
        
        return response()->json($task, 201);
    }

    public function delete($id)
    {
        try {
            $task = Task::findOrFail($id);

            $task->delete();
 
            return response()->json('Removed successfully.');
        } catch (ModelNotFoundException $exception) {
            return response()->json(['error' => "Task with id {$id} not found."], 404);
        }
    }
}
